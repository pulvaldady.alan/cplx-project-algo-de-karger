import random
from Graphes import *
import copy
import time
import math

class Instance:
    def __init__(self, I):
        self.Instance = I


class InstanceKarger(Instance):
    def __init__(self, multi_graphe, nb_sommets):
        super().__init__(multi_graphe)
        self.nb_sommets = nb_sommets
        self.save = MultGrapheMatriceAdj([], [], multi_graphe.ver)
        self.G1 = MultGrapheMatriceAdj([], [], multi_graphe.ver)
        self.G2 = MultGrapheMatriceAdj([], [], multi_graphe.ver)
        multi_graphe.copy(self.save)
        multi_graphe.copy(self.G1)
        multi_graphe.copy(self.G2)

    def reini_instanc(self):
        self.Instance = copy.deepcopy(self.save)

    def selection_aleatoire_v2(self):
        # random.seed(time.time())
        d = self.Instance.getCout
        # print(d)
        # print(d)
        res = sum(d.values())
        choix = random.randint(1, res)

        for key, value in d.items():
            choix -= value
            if choix < 0 or choix == 0:
                # print(key)
                return key
        return -1

    def selection_aleatoire_v1(self):
        """
        Fonction de selection aleatoire de contraction, de manière aleatoire nous tirons un nombre @res compris entre 1
        et le nombre total d'arête on selectionne un premier sommet @indice en parcourant la liste des valeurs de ses
        sommets et en soustrayant @res de cette valeur (chaque sommet a pour valeur son nombre total d'arête) une fois que @res
        est plus petit que 0 ou que celui ci est égal a 0 on re donne a @res sa valeur a l'itération @indice-1 et on itère
        cette fois ci sur les arêtes du sommet de @indice (via la matrice d'adjacence) et on décrémente @res. Une fois que @res
        est plus petit ou égale a 0 on selectionne l'arête sur laquelle @res a atteint la condition.
        """
        # random.seed(time.time())
        d = self.Instance.getCout
        res = sum(d.values())
        choix = random.randint(1, res)
        valeurs = list(d.values())
        # print(valeurs)
        # print(choix)

        indice = None
        for i in range(len(valeurs)):
            choix -= valeurs[i]
            if choix < 0 or choix == 0:
                # print(choix, i, valeurs)
                indice = copy.copy(i)
                # print(valeurs[i])
                choix += valeurs[i]
                break
        # print("Id : ",indice)

        matrice_adj = self.Instance.getMatriceAdj
        # print(matrice_adj)
        # print(choix)
        for i in range(self.nb_sommets):
            # print(i)
            try:
                if matrice_adj[indice][i] > 0:
                    choix -= matrice_adj[indice][i]
                    # print(choix)
                    if choix < 0 or choix == 0:
                        # print(indice, i)
                        return indice, i
            except:
                print("ERROR WITH VALUE", indice, i)
                return -1
        return -1

    def contraction_partielle_v1(self, Instance, t):
        nb_contraction = 0
        while nb_contraction < len(Instance.Instance.getV) - t:
            sommets = Instance.selection_aleatoire_ks_v1(Instance)
            if sommets == -1:
                print("Erreur")
                return [0]
            u, v = sommets
            Instance.Instance.contraction_v1(u, v)
            nb_contraction += 1
        D = self.Instance.getDictCorrespondance
        S = self.Instance.getV
        l = list()
        # print(D)

        real_sommet = list()
        # print(S)
        for elem in S:
            try:
                l.append(D[elem.getList()[0]])
                real_sommet.append(elem.getList()[0])
            except KeyError:
                l.append(elem.getList()[0])
                real_sommet.append(elem.getList()[0])

        # print(real_sommet)
        R = self.Instance.getMatriceAdj
        # print(real_sommet[0])
        # print(real_sommet[1])

        if t > 2:
            return l, 0

        taille_coupe = R[real_sommet[0]][real_sommet[1]]

        return l, taille_coupe

    def contraction_partielle_v2(self, Instance, t):
        nb_contraction = 0
        while nb_contraction < len(Instance.Instance.getV) - t:
            sommets = Instance.selection_aleatoire_ks_v2(Instance)
            if sommets == -1:
                print("Erreur")
                return [0]
            u, v = sommets
            Instance.Instance.contraction_v2(u, v)
            nb_contraction += 1

        D = self.Instance.getDictCorrespondance
        S = self.Instance.getV
        E = self.Instance.getE
        l = list()
        C = self.Instance.getCout
        for elem in S:
            try:
                l.append(D[elem])
            except KeyError:
                l.append(elem)
        try:
            cout = C[E[0]]
        except:
            cout = 0

        return l, cout

    def selection_aleatoire_ks_v1(self, I1):
        """
        Fonction de selection aleatoire de contraction, de manière aleatoire nous tirons un nombre @res compris entre 1
        et le nombre total d'arête on selectionne un premier sommet @indice en parcourant la liste des valeurs de ses
        sommets et en soustrayant @res de cette valeur (chaque sommet a pour valeur son nombre total d'arête) une fois que @res
        est plus petit que 0 ou que celui ci est égal a 0 on re donne a @res sa valeur a l'itération @indice-1 et on itère
        cette fois ci sur les arêtes du sommet de @indice (via la matrice d'adjacence) et on décrémente @res. Une fois que @res
        est plus petit ou égale a 0 on selectionne l'arête sur laquelle @res a atteint la condition.
        """
        # random.seed(time.time())
        d = I1.Instance.getCout
        res = sum(d.values())
        choix = random.randint(1, res)
        valeurs = list(d.values())
        # print(valeurs)
        # print(choix)

        indice = None
        for i in range(len(valeurs)):
            choix -= valeurs[i]
            if choix < 0 or choix == 0:
                # print(choix, i, valeurs)
                indice = copy.copy(i)
                # print(valeurs[i])
                choix += valeurs[i]
                break
        # print("Id : ",indice)

        matrice_adj = I1.Instance.getMatriceAdj
        # print(matrice_adj)
        # print(choix)
        for i in range(self.nb_sommets):
            # print(i)
            try:
                if matrice_adj[indice][i] > 0:
                    choix -= matrice_adj[indice][i]
                    # print(choix)
                    if choix < 0 or choix == 0:
                        # print(indice, i)
                        return indice, i
            except:
                print("ERROR WITH VALUE", indice, i)
                return -1
        return -1

    def selection_aleatoire_ks_v2(self, I1):
        """
        Fonction de selection aleatoire de contraction, de manière aleatoire nous tirons un nombre @res compris entre 1
        et le nombre total d'arête on selectionne un premier sommet @indice en parcourant la liste des valeurs de ses
        sommets et en soustrayant @res de cette valeur (chaque sommet a pour valeur son nombre total d'arête) une fois que @res
        est plus petit que 0 ou que celui ci est égal a 0 on re donne a @res sa valeur a l'itération @indice-1 et on itère
        cette fois ci sur les arêtes du sommet de @indice (via la matrice d'adjacence) et on décrémente @res. Une fois que @res
        est plus petit ou égale a 0 on selectionne l'arête sur laquelle @res a atteint la condition.
        """
        # random.seed(time.time())
        # random.seed(time.time())
        d = I1.Instance.getCout
        # print(d)
        # print(d)
        res = sum(d.values())
        choix = random.randint(1, res)

        for key, value in d.items():
            choix -= value
            if choix < 0 or choix == 0:
                # print(key)
                return key
        return -1

    def run_v1(self, t=2):
        random.seed(time.time())
        nb_contraction = 0
        while nb_contraction < self.nb_sommets - t:
            # print("iteration ",nb_contraction)
            sommets = self.selection_aleatoire_v1()
            if sommets == -1:
                print("Erreur")
                return [0]
            u, v = sommets
            self.Instance.contraction_v1(u, v)
            nb_contraction += 1
        D = self.Instance.getDictCorrespondance
        S = self.Instance.getV
        l = list()
        # print(D)

        real_sommet = list()
        # print(S)
        for elem in S:
            try:
                l.append(D[elem.getList()[0]])
                real_sommet.append(elem.getList()[0])
            except KeyError:
                l.append(elem.getList()[0])
                real_sommet.append(elem.getList()[0])

        # print(real_sommet)
        R = self.Instance.getMatriceAdj
        # print(real_sommet[0])
        # print(real_sommet[1])

        if t > 2:
            return l, 0

        taille_coupe = R[real_sommet[0]][real_sommet[1]]

        return l, taille_coupe

    def run_v2(self, t=2):
        random.seed(time.time())
        nb_contraction = 0
        while nb_contraction < self.nb_sommets - t and len(self.Instance.getE) > 1:
            # print(nb_contraction)
            sommets = self.selection_aleatoire_v2()
            if sommets == -1:
                print("Erreur")
                return [0]
            # print(sommets)
            u, v = sommets
            self.Instance.contraction_v2(u, v)
            nb_contraction += 1

        D = self.Instance.getDictCorrespondance
        S = self.Instance.getV
        E = self.Instance.getE
        l = list()
        C = self.Instance.getCout
        for elem in S:
            try:
                l.append(D[elem])
            except KeyError:
                l.append(elem)
        try:
            cout = C[E[0]]
        except:
            cout = 0

        return l, cout

    def run_iterer_v1(self, T):
        random.seed(time.time())
        m = math.inf
        S_star = None
        for i in range(0, T):
            nb_contraction = 0
            self.reini_instanc()
            while nb_contraction < self.nb_sommets - 2:
                sommets = self.selection_aleatoire_v1()
                if sommets == -1:
                    print("Erreur")
                    return [0]
                u, v = sommets
                self.Instance.contraction_v1(u, v)
                nb_contraction += 1
            # Fin while
            D = self.Instance.getDictCorrespondance
            S = self.Instance.getV
            l = list()
            # print(D)

            real_sommet = list()
            # print(S)
            for elem in S:
                try:
                    l.append(D[elem.getList()[0]])
                    real_sommet.append(elem.getList()[0])
                except KeyError:
                    l.append(elem.getList()[0])
                    real_sommet.append(elem.getList()[0])

            # print(real_sommet)
            R = self.Instance.getMatriceAdj
            # print(real_sommet[0])
            # print(real_sommet[1])
            # print(real_sommet)
            taille_coupe = R[real_sommet[0]][real_sommet[1]]
            if taille_coupe < m:
                m = taille_coupe
                S_star = l

        return S_star, m

    def run_iterer_v2(self, T):
        random.seed(time.time())
        m = math.inf
        S_star = None
        for i in range(0, T):
            # print(i)
            self.reini_instanc()
            nb_contraction = 0
            while nb_contraction < self.nb_sommets - 2:
                # print("iteration ",nb_contraction)
                sommets = self.selection_aleatoire_v2()
                if sommets == -1:
                    print("Erreur")
                    return [0]
                # print(self.Instance.getCout)
                u, v = sommets
                self.Instance.contraction_v2(u, v)
                nb_contraction += 1
            # Fin while
            D = self.Instance.getDictCorrespondance
            S = self.Instance.getV
            E = self.Instance.getE
            l = list()
            C = self.Instance.getCout
            for elem in S:
                try:
                    l.append(D[elem])
                except KeyError:
                    l.append(elem)
            # print(C)
            if C[E[0]] < m:
                m = C[E[0]]
                S_star = l

        return S_star, m

    def recherche_exhaustive_v2(self, Graphe):
        # print("--------")
        coupe_min = math.inf
        liste_coupe_min = list()
        V = Graphe.getV
        E = Graphe.getE
        C = Graphe.getCout
        contraction_possible = list()
        if len(V) > 2:
            for u in V:
                for v in V:
                    if u == v:
                        continue
                    if (u, v) in contraction_possible:
                        continue
                    if (v, u) in contraction_possible:
                        continue
                    if (u, v) in C.keys():
                        if C[(u, v)] == 0:
                            continue
                    contraction_possible.append((u, v))

            # print(len(contraction_possible), V)
            for elem in contraction_possible:
                G_copy = MultGrapheMatriceAdj([], [], 2)
                Graphe.copy(G_copy)
                u, v = elem
                # print("contraction de {} et {}".format(u, v))
                G_copy.contraction_v2(u, v)
                liste_sommet_restant, m = self.recherche_exhaustive_v2(G_copy)
                if m < coupe_min:
                    coupe_min = m
                    liste_coupe_min = liste_sommet_restant
                if coupe_min == 1:
                    return liste_sommet_restant, coupe_min

                # print(liste_sommet_restant, coupe_min)

        if len(V) == 2:
            # print("Affichage de V - ", V)
            u = V[0]
            # print("u :", u)
            v = V[1]
            # print("v :", v)
            m = C[E[0]]
            # print("m : ", m)
            liste_sommet_restant = V
            return liste_sommet_restant, m

        return liste_coupe_min, coupe_min

    def recherche_exhaustive_v1(self, Graphe):
        # print("--------")
        coupe_min = math.inf
        liste_coupe_min = list()
        V = Graphe.getV
        R = Graphe.getMatriceAdj
        contraction_possible = list()
        if len(V) > 2:
            for u in [elem.getList()[0] for elem in V]:
                for v in [elem.getList()[0] for elem in V]:
                    if u == v:
                        continue
                    if (u, v) in contraction_possible:
                        continue
                    if (v, u) in contraction_possible:
                        continue
                    if R[u][v] == 0:
                        continue
                    contraction_possible.append((u, v))
            for elem in contraction_possible:
                G_copy = MultGrapheMatriceAdj([], [], 1)
                Graphe.copy(G_copy)
                u, v = elem
                # print("contraction de {} et {}".format(u,v))
                G_copy.contraction_v1(u, v)
                liste_sommet_restant, m = self.recherche_exhaustive_v1(G_copy)
                if m < coupe_min:
                    coupe_min = m
                    liste_coupe_min = liste_sommet_restant
                if coupe_min == 1:
                    return liste_sommet_restant, coupe_min

                # print(liste_sommet_restant, coupe_min)

        if len(V) == 2:
            # print("Affichage de V - ", [elem.getList()[0] for elem in V])
            u = V[0].getList()[0]
            # print("u :", u)
            v = V[1].getList()[0]
            # print("v :", v)
            m = R[u][v]
            # print("m : ", m)
            liste_sommet_restant = [elem.getList()[0] for elem in V]
            return liste_sommet_restant, m

        return liste_coupe_min, coupe_min

    def run_karger_stein_v1(self, noeud=0):
        if noeud == 0:
            V = self.Instance.getV
            I = self.Instance
        if noeud == 1:
            V = self.G1.getV
            I = self.G1
        else:
            V = self.G2.getV
            I = self.G2

        if len(V) <= 6:
            # print("Recherche ehaustive")
            m = self.recherche_exhaustive_v1(I)
            # print(m)
            return m

        if len(V) > 6:
            t = math.ceil(1 + (len(V) / math.sqrt(2)))
            # print(t)
            I1 = InstanceKarger(self.G1, self.G1.nb_sommet)
            self.contraction_partielle_v1(I1, t)
            s1, m1 = self.run_karger_stein_v1(1)
            I2 = InstanceKarger(self.G2, self.G1.nb_sommet)
            self.contraction_partielle_v1(I2, t)
            s2, m2 = self.run_karger_stein_v1(2)
            if m1 < m2:
                return s1, m1
            else:
                return s2, m2

    def run_karger_stein_v2(self, noeud=0):
        if noeud == 0:
            V = self.Instance.getV
            I = self.Instance
        if noeud == 1:
            V = self.G1.getV
            I = self.G1
        else:
            V = self.G2.getV
            I = self.G2

        if len(V) <= 6:
            # print("Recherche ehaustive")
            m = self.recherche_exhaustive_v2(I)
            # print(m)
            return m

        if len(V) > 6:
            t = math.ceil(1 + (len(V) / math.sqrt(2)))
            I1 = InstanceKarger(self.G1, self.G1.nb_sommet)
            self.contraction_partielle_v2(I1, t)
            s1, m1 = self.run_karger_stein_v2(1)
            I2 = InstanceKarger(self.G2, self.G1.nb_sommet)
            self.contraction_partielle_v2(I2, t)
            s2, m2 = self.run_karger_stein_v2(2)
            if m1 < m2:
                return s1, m1
            else:
                return s2, m2
