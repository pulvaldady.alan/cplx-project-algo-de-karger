import matplotlib.pyplot as plt
from Tests import *
from Graphes import *
from Representations import *


# test_contraction_graphe_cyclique()
# test_contraction_graphe_alea()
# test_contraction_graphe_alea_cyclique()
def Question2a_v1(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G1 = MultGrapheMatriceAdj([0, 1, 2, 3, 4], [(0, 1), (0, 3), (3, 2), (1, 4), (3, 1), (2, 4)], 1)
        T = InstanceKarger(G1, 5)
        l, c = T.run_v1()
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 2:
            nb_succes += 1
    return nb_succes / iterations


def Question2a_v2(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G1 = MultGrapheMatriceAdj([0, 1, 2, 3, 4], [(0, 1), (0, 3), (3, 2), (1, 4), (3, 1), (2, 4)], 2)
        T = InstanceKarger(G1, 5)
        l, c = T.run_v2()
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 2:
            nb_succes += 1
    return nb_succes / iterations


def Question2a_v1_2(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G2 = MultGrapheMatriceAdj([0, 1, 2, 3, 4, 5, 6, 7],
                                  [(0, 1), (0, 3), (0, 2), (1, 2), (1, 3), (2, 3), (1, 4), (3, 4), (4, 5), (4, 6),
                                   (4, 7), (5, 6), (5, 7), (6, 7)], 1)
        T = InstanceKarger(G2, 8)
        l, c = T.run_v1()
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 2:
            nb_succes += 1
    return nb_succes / iterations


def Question2a_v2_2(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G2 = MultGrapheMatriceAdj([0, 1, 2, 3, 4, 5, 6, 7],
                                  [(0, 1), (0, 3), (0, 2), (1, 2), (1, 3), (2, 3), (1, 4), (3, 4), (4, 5), (4, 6),
                                   (4, 7), (5, 6), (5, 7), (6, 7)], 2)
        T = InstanceKarger(G2, 8)
        l, c = T.run_v2()
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 2:
            nb_succes += 1
    return nb_succes / iterations


def Question2a_v1_3(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G = GEN_COMPLET_GRAPHE(10, 1)
        T = InstanceKarger(G, 10)
        l, c = T.run_v1()
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 9:
            nb_succes += 1
    return nb_succes / iterations


def Question2a_v2_3(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G = GEN_COMPLET_GRAPHE(10, 2)
        T = InstanceKarger(G, 10)
        l, c = T.run_v2()
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 9:
            nb_succes += 1
    return nb_succes / iterations


def Question2c_v1(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G1 = MultGrapheMatriceAdj([0, 1, 2, 3, 4], [(0, 1), (0, 3), (3, 2), (1, 4), (3, 1), (2, 4)], 1)
        T = InstanceKarger(G1, 5)
        l, c = T.run_iterer_v1(10)
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 2:
            nb_succes += 1
    return nb_succes / iterations


def Question2c_v2(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G1 = MultGrapheMatriceAdj([0, 1, 2, 3, 4], [(0, 1), (0, 3), (3, 2), (1, 4), (3, 1), (2, 4)], 2)
        T = InstanceKarger(G1, 5)
        l, c = T.run_iterer_v2(10)
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 2:
            nb_succes += 1
    return nb_succes / iterations


def Question2c_v1_2(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G2 = MultGrapheMatriceAdj([0, 1, 2, 3, 4, 5, 6, 7],
                                  [(0, 1), (0, 3), (0, 2), (1, 2), (1, 3), (2, 3), (1, 4), (3, 4), (4, 5), (4, 6),
                                   (4, 7), (5, 6), (5, 7), (6, 7)], 1)
        T = InstanceKarger(G2, 8)
        l, c = T.run_iterer_v1(10)
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 2:
            nb_succes += 1
    return nb_succes / iterations


def Question2c_v2_2(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G2 = MultGrapheMatriceAdj([0, 1, 2, 3, 4, 5, 6, 7],
                                  [(0, 1), (0, 3), (0, 2), (1, 2), (1, 3), (2, 3), (1, 4), (3, 4), (4, 5), (4, 6),
                                   (4, 7), (5, 6), (5, 7), (6, 7)], 2)
        T = InstanceKarger(G2, 8)
        l, c = T.run_iterer_v2(10)
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 2:
            nb_succes += 1
    return nb_succes / iterations


def Question2c_v1_3(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G = GEN_COMPLET_GRAPHE(10, 1)
        T = InstanceKarger(G, 10)
        l, c = T.run_iterer_v1(10)
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 9:
            nb_succes += 1
    return nb_succes / iterations


def Question2c_v2_3(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G = GEN_COMPLET_GRAPHE(10, 2)
        T = InstanceKarger(G, 10)
        l, c = T.run_iterer_v2(10)
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 9:
            nb_succes += 1
    return nb_succes / iterations


def Question3e_v1(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G1 = MultGrapheMatriceAdj([0, 1, 2, 3, 4], [(0, 1), (0, 3), (3, 2), (1, 4), (3, 1), (2, 4)], 1)
        T = InstanceKarger(G1, 5)
        l, c = T.run_karger_stein_v1()
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 2:
            nb_succes += 1
    return nb_succes / iterations


def Question3e_v2(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        G1 = MultGrapheMatriceAdj([0, 1, 2, 3, 4], [(0, 1), (0, 3), (3, 2), (1, 4), (3, 1), (2, 4)], 2)
        T = InstanceKarger(G1, 5)
        l, c = T.run_karger_stein_v2()
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 2:
            nb_succes += 1
    return nb_succes / iterations


def Question3e_v1_2(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        print(i)
        G2 = MultGrapheMatriceAdj([0, 1, 2, 3, 4, 5, 6, 7],
                                  [(0, 1), (0, 3), (0, 2), (1, 2), (1, 3), (2, 3), (1, 4), (3, 4), (4, 5), (4, 6),
                                   (4, 7), (5, 6), (5, 7), (6, 7)], 1)
        T = InstanceKarger(G2, 8)
        l, c = T.run_karger_stein_v1()
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 2:
            nb_succes += 1
    return nb_succes / iterations


def Question3e_v2_2(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        print(i)
        G2 = MultGrapheMatriceAdj([0, 1, 2, 3, 4, 5, 6, 7],
                                  [(0, 1), (0, 3), (0, 2), (1, 2), (1, 3), (2, 3), (1, 4), (3, 4), (4, 5), (4, 6),
                                   (4, 7), (5, 6), (5, 7), (6, 7)], 2)
        T = InstanceKarger(G2, 8)
        l, c = T.run_karger_stein_v2()
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 2:
            nb_succes += 1
    return nb_succes / iterations


def Question3e_v1_3(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        print(i)
        G = GEN_COMPLET_GRAPHE(10, 1)
        T = InstanceKarger(G, 10)
        l, c = T.run_karger_stein_v1()
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 9:
            nb_succes += 1
    return nb_succes / iterations


def Question3e_v2_3(iterations):
    nb_succes = 0
    for i in range(0, iterations):
        print(i)
        G = GEN_COMPLET_GRAPHE(10, 2)
        T = InstanceKarger(G, 10)
        l, c = T.run_karger_stein_v2()
        r = Res(l, c)
        # r.humanRepresentation()
        if r.getTailleCoupe == 9:
            nb_succes += 1
    return nb_succes / iterations


def TEST_QUESTION_2a():
    print("Probabilite de renvoyer la bonne coupe pour la version 1 {} % ".format((Question2a_v1(10000) * 100)))
    print("Probabilite de renvoyer la bonne coupe pour la version 2 {} % ".format((Question2a_v2(10000) * 100)))

    print("Probabilite de renvoyer la bonne coupe pour la version 1 (Graphe exemple du cours) {} % ".format(
        (Question2a_v1_2(10000) * 100)))
    print("Probabilite de renvoyer la bonne coupe pour la version 2 (Graphe exemple du cours) {} % ".format(
        (Question2a_v2_2(10000) * 100)))

    print("Probabilite de renvoyer la bonne coupe pour la version 1 (Graphe complet) {} % ".format(
        (Question2a_v1_3(10000) * 100)))
    print("Probabilite de renvoyer la bonne coupe pour la version 2 (Graphe complet) {} % ".format(
        (Question2a_v2_3(10000) * 100)))




def TEST_QUESTION_2c():
    #print("Probabilite de renvoyer la bonne coupe pour la version 1 {} % ".format((Question2c_v1(10000) * 100)))
    #print("Probabilite de renvoyer la bonne coupe pour la version 2 {} % ".format((Question2c_v2(10000) * 100)))

    #print("Probabilite de renvoyer la bonne coupe pour la version 1 (Graphe exemple du cours) {} % ".format(
    #    (Question2c_v1_2(10000) * 100)))
    #print("Probabilite de renvoyer la bonne coupe pour la version 2 (Graphe exemple du cours) {} % ".format(
    #    (Question2c_v2_2(10000) * 100)))

    print("Probabilite de renvoyer la bonne coupe pour la version 1 (Graphe complet) {} % ".format(
        (Question2c_v1_3(10000) * 100)))
    print("Probabilite de renvoyer la bonne coupe pour la version 2 (Graphe complet) {} % ".format(
        (Question2c_v2_3(10000) * 100)))


def TEST_QUESTION_3e():
    #print("Probabilite de renvoyer la bonne coupe pour la version 1 {} % ".format((Question3e_v1(10000) * 100)))
    #print("Probabilite de renvoyer la bonne coupe pour la version 2 {} % ".format((Question3e_v2(10000) * 100)))

    #print("Probabilite de renvoyer la bonne coupe pour la version 1 (Graphe exemple du cours) {} % ".format(
    #    (Question3e_v1_2(30) * 100)))
    #print("Probabilite de renvoyer la bonne coupe pour la version 2 (Graphe exemple du cours) {} % ".format(
    #    (Question3e_v2_2(30) * 100)))

    print("Probabilite de renvoyer la bonne coupe pour la version 1 (Graphe complet) {} % ".format(
        (Question3e_v1_3(100) * 100)))
    print("Probabilite de renvoyer la bonne coupe pour la version 2 (Graphe complet) {} % ".format(
        (Question3e_v2_3(100) * 100)))



#print("------------Question 2a-----------")
#TEST_QUESTION_2a()
#print("------------Question 2c-----------")
#TEST_QUESTION_2c()
#print("------------Question 3e-----------")
#TEST_QUESTION_3e()


def TEST_COMPLEXITE_KARGER(nb_iteration):
    A = 0#test_contraction_graphe_alea(nb_iteration)
    B = 0#test_contraction_graphe_cyclique(nb_iteration)
    C = 0#test_contraction_graphe_alea_cyclique(nb_iteration)
    D = 0#test_contraction_graphe_biparti(nb_iteration)
    E = test_contraction_graphe_complet(nb_iteration)
    return A, B, C, D, E


def TEST_COMPLEXITE_KARGER_ITER():
    pass


def TEST_COMPLEXITE_KARGER_STEIN():
    pass

def afficheGraphe(T, titre, x_label, y_label, nb_iteration):
    plt.title(titre)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.plot([x for x in range(100, nb_iteration, 100)], T)
    plt.show()

def afficheMultGraphe(T,T2, titre, x_label, y_label, nb_iteration):
    plt.title(titre)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.plot([x for x in range(100, nb_iteration, 100)], T, color='r', label='Karger v1')
    plt.plot([x for x in range(100, nb_iteration, 100)], T2, color='g', label='Karger v2')
    plt.show()

def tests_Complexite_contraction():
    n = 500
    a, b, c, d, e = TEST_COMPLEXITE_KARGER(n)
    l1, l2 = e
    label_x = "Nombre de sommet"
    label_y = "Temps d'exécution en seconde"
    afficheGraphe(l1, "Graphique du temps d'exécution en fonction des sommets", label_x, label_y, n)
    afficheGraphe(l2, "Graphique du temps d'exécution en fonction des sommets", label_x, label_y, n)

def test_complexite_karger(n):
    a = test_karger_graphe_alea(n)
    #b = test_karger_graphe_biparti(n)
    #c = test_karger_graphe_cyclique(n)
    #d = test_karger_graphe_alea_cyclique(n)
    #e = test_karger_graphe_complet(n)
    l1, l2 = a
    #l3, l4 = b
    #l5, l6 = c
    #l7, l8 = d
    #l9, l10 = e
    label_x = "Nombre de sommet"
    label_y = "Temps d'exécution en seconde"
    afficheGraphe(l1, "Graphique du temps d'exécution en fonction des sommets de Karger(Graphe alea)", label_x, label_y, n)
    #afficheGraphe(l3, "Graphique du temps d'exécution en fonction des sommets de Karger(Graphe Biparti)", label_x, label_y, n)
    #afficheGraphe(l5, "Graphique du temps d'exécution en fonction des sommets de Karger(Graphe cyclique)", label_x, label_y, n)
    #afficheGraphe(l7, "Graphique du temps d'exécution en fonction des sommets de Karger(Graphe alea cyclique)", label_x, label_y, n)
    #afficheGraphe(l9, "Graphique du temps d'exécution en fonction des sommets de Karger(Graphe complet)", label_x, label_y, n)

def test_complexite_karger_iterer(n):
    a = test_karger_it_graphe_alea(n)
    b = test_karger_it_graphe_biparti(n)
    c = test_karger_it_graphe_cyclique(n)
    d = test_karger_it_graphe_alea_cyclique(n)
    e = test_karger_it_graphe_complet(n)
    l1, l2 = a
    l3, l4 = b
    l5, l6 = c
    l7, l8 = d
    l9, l10 = e
    label_x = "Nombre de sommet"
    label_y = "Temps d'exécution en seconde"
    afficheGraphe(l1, "Graphique du temps d'exécution en fonction des sommets de Karger Itéré (Graphe alea)", label_x, label_y, n)
    afficheGraphe(l3, "Graphique du temps d'exécution en fonction des sommets de Karger Itéré (Graphe Biparti)", label_x, label_y, n)
    afficheGraphe(l5, "Graphique du temps d'exécution en fonction des sommets de Karger Itéré (Graphe cyclique)", label_x, label_y, n)
    afficheGraphe(l7, "Graphique du temps d'exécution en fonction des sommets de Karger Itéré (Graphe alea cyclique)", label_x, label_y, n)
    afficheGraphe(l9, "Graphique du temps d'exécution en fonction des sommets de Karger Itéré (Graphe complet)", label_x, label_y, n)

def test_complexite_karger_stein(n):
    #a = test_karger_stein_graphe_alea(n)
    #b = test_karger_stein_graphe_biparti(n)
    #c = test_karger_stein_graphe_cyclique(n)
    #d = test_karger_stein_graphe_alea_cyclique(n)
    e = test_karger_stein_graphe_complet(n)
    #l1, l2 = a
    #l3, l4 = b
    #l5, l6 = c
    #l7, l8 = d
    l9, l10 = e
    label_x = "Nombre de sommet"
    label_y = "Temps d'exécution en seconde"
    #afficheGraphe(l1, "Graphique du temps d'exécution en fonction des sommets de Karger-Stein (Graphe alea)", label_x, label_y, n)

    #afficheGraphe(l3, "Graphique du temps d'exécution en fonction des sommets de Karger-Stein (Graphe Biparti)", label_x, label_y, n)
    #afficheGraphe(l5, "Graphique du temps d'exécution en fonction des sommets de Karger-Stein (Graphe cyclique)", label_x, label_y, n)
    #afficheGraphe(l7, "Graphique du temps d'exécution en fonction des sommets de Karger-Stein (Graphe alea cyclique)", label_x, label_y, n)
    afficheGraphe(l9, "Graphique du temps d'exécution en fonction des sommets de Karger-Stein (Graphe complet)", label_x, label_y, n)


#tests_Complexite_contraction()
#test_complexite_karger_stein(100)
#test_complexite_karger(1000)