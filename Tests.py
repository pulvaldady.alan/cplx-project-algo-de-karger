import time
from Instance import *
from Graphes import *


def GEN_RANDOM_GRAPHE(N, ver):
    random.seed(time.time())
    L = list()
    E = list()
    for i in range(0, N):
        L.append(i)

    for u in L:
        for v in L:
            if v != u:
                if (v, u) not in E:
                    if random.random() < 0.6:
                        E.append((u, v))

    return MultGrapheMatriceAdj(L, E, ver)


def GEN_CYCLIC_ALEA_GRAPHE(N, ver):
    random.seed(time.time())
    L = list()
    E = list()
    for i in range(0, N):
        E.append((i, ((i + 1) % N)))
        L.append(i)

    for u in L:
        for v in L:
            if v != u:
                if random.random() < 0.6:
                    E.append((u, v))
    return MultGrapheMatriceAdj(L, E, ver)


def GEN_CYCLIC_GRAPHE(N, ver):
    random.seed(time.time())
    L = list()
    E = list()
    for i in range(0, N):
        E.append((i, ((i + 1) % N)))
        L.append(i)

    return MultGrapheMatriceAdj(L, E, ver)


def GEN_COMPLET_GRAPHE(N, ver):
    random.seed(time.time())
    L = list()
    E = list()
    for i in range(0, N):
        L.append(i)

    for u in L:
        for v in L:
            if v != u:
                if (v, u) not in E:
                    E.append((u, v))

    return MultGrapheMatriceAdj(L, E, ver)


def GEN_BIPARTI_COMPLET_GRAPHE(N, ver):
    random.seed(time.time())
    L = list()
    E = list()
    for i in range(0, N):
        L.append(i)

    for u in L:
        for v in L:
            if v != u:
                if (v, u) not in E:
                    if v % 2 == u % 2:
                        E.append((u, v))

    return MultGrapheMatriceAdj(L, E, ver)


def test_contraction_graphe_alea(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_RANDOM_GRAPHE(i, 1)
        G_r2 = GEN_RANDOM_GRAPHE(i, 2)
        # print(G_r.getMatriceAdj)
        # print(G_r.getCout)
        I1 = InstanceKarger(G_r, i)
        I2 = InstanceKarger(G_r2, i)
        x1, y1 = I1.selection_aleatoire_v1()
        x2, y2 = I2.selection_aleatoire_v2()
        d = time.time()
        G_r.contraction_v1(x1, y1)
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        G_r2.contraction_v2(x2, y2)
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_contraction_graphe_alea_cyclique(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_CYCLIC_ALEA_GRAPHE(i, 1)
        G_r2 = GEN_CYCLIC_ALEA_GRAPHE(i, 2)
        # print(G_r.getMatriceAdj)
        # print(G_r.getCout)
        I1 = InstanceKarger(G_r, i)
        I2 = InstanceKarger(G_r2, i)
        x1, y1 = I1.selection_aleatoire_v1()
        x2, y2 = I2.selection_aleatoire_v2()
        d = time.time()
        G_r.contraction_v1(x1, y1)
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        G_r2.contraction_v2(x2, y2)
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_contraction_graphe_cyclique(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_CYCLIC_GRAPHE(i, 1)
        G_r2 = GEN_CYCLIC_GRAPHE(i, 2)
        # print(G_r.getMatriceAdj)
        # print(G_r.getCout)
        I1 = InstanceKarger(G_r, i)
        I2 = InstanceKarger(G_r2, i)
        x1, y1 = I1.selection_aleatoire_v1()
        x2, y2 = I2.selection_aleatoire_v2()
        d = time.time()
        G_r.contraction_v1(x1, y1)
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        G_r2.contraction_v2(x2, y2)
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_contraction_graphe_complet(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_COMPLET_GRAPHE(i, 1)
        G_r2 = GEN_COMPLET_GRAPHE(i, 2)
        # print(G_r.getMatriceAdj)
        # print(G_r.getCout)
        I1 = InstanceKarger(G_r, i)
        I2 = InstanceKarger(G_r2, i)
        x1, y1 = I1.selection_aleatoire_v1()
        x2, y2 = I2.selection_aleatoire_v2()
        d = time.time()
        G_r.contraction_v1(x1, y1)
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        G_r2.contraction_v2(x2, y2)
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_contraction_graphe_biparti(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_BIPARTI_COMPLET_GRAPHE(i, 1)
        G_r2 = GEN_BIPARTI_COMPLET_GRAPHE(i, 2)
        # print(G_r.getMatriceAdj)
        # print(G_r.getCout)
        I1 = InstanceKarger(G_r, i)
        I2 = InstanceKarger(G_r2, i)
        x1, y1 = I1.selection_aleatoire_v1()
        x2, y2 = I2.selection_aleatoire_v2()
        d = time.time()
        G_r.contraction_v1(x1, y1)
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        G_r2.contraction_v2(x2, y2)
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_graphe_alea(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_RANDOM_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_v1()
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_graphe_alea_cyclique(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_CYCLIC_ALEA_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_v1()
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_graphe_cyclique(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_CYCLIC_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_v1()
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_graphe_complet(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_COMPLET_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_v1()
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_graphe_biparti(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_BIPARTI_COMPLET_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_v1()
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_it_graphe_alea(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_RANDOM_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_iterer_v1(10)
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_it_graphe_alea_cyclique(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_CYCLIC_ALEA_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_iterer_v1(10)
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_it_graphe_cyclique(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_CYCLIC_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_iterer_v1(10)
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_it_graphe_complet(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_COMPLET_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_iterer_v1(10)
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_it_graphe_biparti(M):
    L1 = list()
    L2 = list()
    for i in range(100, M, 100):
        print(i)
        G_r = GEN_BIPARTI_COMPLET_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_iterer_v1(10)
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_stein_graphe_alea(M):
    L1 = list()
    L2 = list()
    for i in range(4, M, 25):
        print(i)
        G_r = GEN_RANDOM_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_karger_stein_v1()
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_stein_graphe_alea_cyclique(M):
    L1 = list()
    L2 = list()
    for i in range(4, M, 25):
        print(i)
        G_r = GEN_CYCLIC_ALEA_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_karger_stein_v1()
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_stein_graphe_cyclique(M):
    L1 = list()
    L2 = list()
    for i in range(4, M, 25):
        print(i)
        G_r = GEN_CYCLIC_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_karger_stein_v1()
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_stein_graphe_complet(M):
    L1 = list()
    L2 = list()
    for i in range(4, M, 25):
        print(i)
        G_r = GEN_COMPLET_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_karger_stein_v1()
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2


def test_karger_stein_graphe_biparti(M):
    L1 = list()
    L2 = list()
    for i in range(4, M, 25):
        print(i)
        G_r = GEN_BIPARTI_COMPLET_GRAPHE(i, 1)
        I1 = InstanceKarger(G_r, i)
        d = time.time()
        I1.run_karger_stein_v1()
        f = time.time()
        r = f - d
        L1.append(r)
        d = time.time()
        print("ignore")
        f = time.time()
        r = f - d
        L2.append(r)
    return L1, L2

# L1 = test_contraction_graphe_alea(1000, 1)
# L2 = test_contraction_graphe_alea_cyclique(1000, 1)
# L3 = test_contraction_graphe_cyclique(1000, 1)
# print("Test 1\n", L1)
# print("Test 2\n", L2)
# print("Test 3\n", L3)
# G1 = MultGrapheMatriceAdj([0, 1, 2, 3, 4,5,6,7], [(0, 1), (0, 3),(0,5),(0,6),(0,7), (3, 2), (1, 4), (3, 1), (2, 4)], 1)

# G1.contraction_v2(0, 3)
# G1.contraction_v2(3, 1)
# G1.contraction_v2(1, 2)
# G1.contraction_v1(3, 4)
# T = InstanceKarger(G1, 8)
# print(T.contraction_partielle_v1(T, 4))
# print(T.run_karger_stein_v1())
# print("Res :", T.recherche_exhaustive_v1(G1))
# l, c = T.run_iterer_v2(20)
# r = Res(l, c)
# r.humanRepresentation()
