import copy
from Representations import *


class Graphe:
    def __init__(self, sommets, aretes):
        self.V = copy.deepcopy(sommets)
        self.E = copy.deepcopy(aretes)
        self.R = 0


class MultGrapheMatriceAdj(Graphe):
    def __init__(self, sommets, aretes, ver):
        super().__init__(sommets, aretes)
        self.correspondance = dict()
        self.buildR()
        self.cout = dict()
        self.nb_sommet = len(self.V)
        self.ver = ver
        self.save_sommmet = copy.deepcopy(self.V)

        if ver == 1:
            self.V = [Ensemble([elem]) for elem in self.V]
            self.updateR_v1()
        if ver == 2:
            self.update_fct_cout()

    def copy(self, I):
        V2 = copy.deepcopy(self.V)
        if self.ver == 1:
            E2 = copy.deepcopy(self.E)
        Coress = copy.deepcopy(self.correspondance)
        R = copy.deepcopy(self.R)
        C = copy.deepcopy(self.cout)
        ver = self.ver
        nb_sommet = self.nb_sommet
        save_s = copy.deepcopy(self.save_sommmet)
        I.V = V2
        if ver == 1:
            I.E = E2
        I.correspondance = Coress
        I.R = R
        I.cout = C
        I.ver = ver
        I.nb_sommet = nb_sommet
        I.save_sommet = save_s

    def change_ver(self):
        self.update_fct_cout()
        self.V = self.save_sommmet

    def update_critic_variable(self, c, corr, nb_sommet, r):
        self.cout = c
        self.correspondance = corr
        self.nb_sommet = nb_sommet
        self.R = r

    def reniclass(self, save):
        sommet = copy.deepcopy(save.getV)
        aretes = copy.deepcopy(save.getE)
        cout = copy.deepcopy(save.getCout)
        correspondance = copy.deepcopy(save.getDictCorrespondance)
        nb_sommet = save.getNbSommets
        R = copy.deepcopy(save.getMatriceAdj)
        self.update_critic_variable(cout, correspondance, nb_sommet, R)
        return 1

    @property
    def getNbSommets(self):
        return len(self.V)

    @property
    def getE(self):
        return self.E

    @property
    def getCout(self):
        # print(self.cout)
        return self.cout

    @property
    def getDictCorrespondance(self):
        return self.correspondance

    @property
    def getMatriceAdj(self):
        return self.R

    @property
    def getV(self):
        return self.V

    def buildR(self):
        self.R = [[0 for i in range(len(self.V))] for i in range(len(self.V))]
        for elem in self.E:
            u, v = elem
            self.R[u][v] = 1
            self.R[v][u] = 1

    def updateR_v1(self):
        for elem in self.V:
            self.cout[elem.getList()[0]] = 0

        for elem in self.E:
            u, v = elem
            self.cout[u] += 1
            self.cout[v] += 1

    def updateR_contract(self, elim):
        for i in range(len(self.V)):
            self.R[elim][i] = 0
        for elem in self.E:
            u, v = elem
            self.R[u][v] = 1
            self.R[u][elim] = 0
            self.R[elim][u] = 0
            self.R[v][u] = 1

    def update_fct_cout(self):
        self.cout = dict()
        for elem in self.E:
            if elem in self.cout.keys():
                self.cout[elem] += 1
            else:
                self.cout[elem] = 1

    def arete_correction(self):
        for elem in self.E:
            x, y = elem
            try:
                self.E.remove((y, x))
                self.E.append(elem)
            except ValueError:
                continue

    def updateCorrespondance(self, u, v):
        u_prim = Ensemble(u)
        if v not in self.correspondance.keys():
            self.correspondance[v] = Ensemble([v])
            if u in self.correspondance.keys():
                self.correspondance[v].union(self.correspondance[u])
            else:
                self.correspondance[v].add(u)
        else:
            if u in self.correspondance.keys():
                self.correspondance[v].union(self.correspondance[u])
            else:
                self.correspondance[v].add(u)

    def contraction_v1(self, u, v):
        """
        Contraction en O(n), en utilisant seulement la matrice d'adjacence
        :param u: sommet 1
        :param v: sommet 2
        :return: Pas de return mais on met a jour la matrice d'adjacence
        """
        u_in_coress = True
        # print("Contraction de {} et {}".format(u, v))
        # print("Les sommets sont : ", [elem.getList() for elem in self.V])
        # print("Les correspondance sont : ", [elem.getList() for elem in self.correspondance.values()])

        if v not in self.correspondance.keys():
            self.correspondance[v] = Ensemble([v])

        if v not in self.cout.keys():
            self.cout[v] = 0

        for elem in self.V:
            # print(elem)
            try:
                if elem.isEqual(Ensemble([u])):
                    self.correspondance[v].union(self.correspondance[u])
                    # self.V.remove(elem)
                    # print(v)
                    # self.correspondance[v].printE()

            except KeyError:
                try:
                    if elem.isEqual(Ensemble([u])):
                        self.correspondance[v].union(elem)
                        # self.V.remove(elem)
                        # print(self.V)
                        # print(v)
                        # self.correspondance[v].printE()
                except AttributeError:
                    pass

        for i in range(0, self.nb_sommet):
            if self.R[u][i] > 0:
                self.R[v][i] += self.R[u][i]

        for i in range(0, self.nb_sommet):
            if self.R[i][u] > 0:
                self.cout[i] -= self.R[i][u]
                self.cout[i] -= self.R[i][v]
                self.R[i][v] += self.R[i][u]
                self.cout[i] += self.R[i][v]
                self.R[i][u] = 0

        self.cout[u] = 0
        self.R[v][v] = 0
        self.cout[v] = 0
        for i in range(0, self.nb_sommet):
            self.cout[v] += self.R[v][i]

        for elem in self.V:
            try:
                if elem.isEqual(u):
                    self.V.remove(u)
            except AttributeError:
                if elem.isIntEqual(u):
                    self.V.remove(elem)

        for i in range(0, self.nb_sommet):
            self.R[u][i] = 0

        self.R[u][v] = 0
        self.R[v][u] = 0

        # print(self.cout)

        # print("Contraction de {} et {} terminé".format(u, v))
        # print("Les sommets sont : ", [elem.getList() for elem in self.V])
        # print("Les correspondance sont : ", [elem.getList() for elem in self.correspondance.values()])

    def contraction_v2(self, u, v):
        """Contraction en O(m + 3m) => O(m)
        -> On met a jours les arêtes en remplaçant les arêtes (u, x) ou (x, u) en (v, x) ou (x, v)
         // Il ne manque plus qu'a mettre a jour les correspondances pour les sommets !
        """
        """
        print("Contraction de {} et {}".format(u, v))
        print("Les sommets sont : ", self.V)
        print("Les correspondance sont : ", self.correspondance)
        """
        try:
            self.E.remove((u, v))  # On retire les liens entre u et v
        except ValueError:
            pass
        try:
            self.E.remove((v, u))  # On retire les liens entre u et v
        except ValueError:
            pass

        new_liste_aretes = list()  # Nouvelle liste des arêtes
        for elem in self.E:
            x, y = elem
            if x == u:
                # Si une arête était en provenance de u on la remplace la provenance par v le nouveau sommet
                new_liste_aretes.append((v, y))
                continue
            if y == u:
                # Si une arête était en destination de u on la remplace la destination par v le nouveau sommet
                new_liste_aretes.append((x, v))
                continue
            new_liste_aretes.append((x, y))

        try:
            while True:
                new_liste_aretes.remove((u, v))  # On retire les liens entre u et v
        except ValueError:
            pass
        try:
            while True:
                new_liste_aretes.remove((v, u))  # On retire les liens entre u et v
        except ValueError:
            pass
        try:
            while True:
                new_liste_aretes.remove((v, v))  # On retire les liens entre u et v
        except ValueError:
            pass
        try:
            while True:
                new_liste_aretes.remove((u, u))  # On retire les liens entre u et v
        except ValueError:
            pass

        # print(new_liste_aretes)
        self.E = new_liste_aretes
        # On remplace les arêtes symetrique par deux fois une même arête O(m) avec m Card(E)
        self.arete_correction()
        # Mise a jour de R (Matrice d'adjacence) en O(m)
        # self.updateR_contract(u)
        # On met a jour les poids de chaque arêtes O(m)
        self.update_fct_cout()
        # Mise a jour de correspondance -> 3 est en reel par exemple {1,2,3} O(m)
        self.updateCorrespondance(u, v)
        # print(u, v)
        self.V.remove(u)

        """print("Contraction de {} et {} termine".format(u, v))
        print("Les sommets sont : ", self.V)
        print("Les correspondance sont : ", [elem.getList() for elem in self.correspondance.values()])
        print(self.cout)
        """
