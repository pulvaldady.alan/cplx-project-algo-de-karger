
class Ensemble:
    def __init__(self, list_elem):
        self.ensemble = list_elem

    def is_in(self, u):
        if u in self.ensemble:
            return True
        return False

    def add(self, elem):
        if elem not in self.ensemble:
            self.ensemble.append(elem)

    def convertListToEnsemble(self, liste):
        res = Ensemble([])
        for elem in liste:
            if elem not in res.ensemble:
                res.ensemble.append(elem)
        return res

    def getList(self):
        return self.ensemble

    def ajoutEnsemble(self, x):
        if x not in self.ensemble:
            self.ensemble.append(x)

    def differenceEnsemble(self, ensemble2):
        return self.convertListToEnsemble(list(set(self.ensemble) - set(ensemble2.getList())))

    def union(self, e2):
        for elem in e2.getList():
            if elem not in self.ensemble:
                self.ensemble.append(elem)

    def printE(self):
        print(self.ensemble)

    def isEqual(self, u):
        for elem in u.getList():
            if elem not in self.ensemble:
                return False
        return True

    def isIntEqual(self, u):
        if len(self.ensemble) > 1:
            return False
        else:
            return self.is_in(u)


class Res(Ensemble):
    def __init__(self, list_elem, taille_coupe):
        super().__init__(list_elem)
        self.taille_coupe = taille_coupe

    def humanRepresentation(self):
        print("Les sommets selectionner sont : ", end="")
        for elem in self.ensemble:
            set_of_vortex = set()
            try:
                for elem2 in elem.getList():
                    set_of_vortex.add(elem2)
            except AttributeError:
                set_of_vortex.add(elem)
            print(set_of_vortex, end=" ")

    @property
    def getTailleCoupe(self):
        return self.taille_coupe
